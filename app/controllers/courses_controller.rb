class CoursesController < ApplicationController
    def index
        @users = User.all
        @courses = Course.all
    end

    def edit
        @course = Course.find(params[:id])
    end

    def update
        @course = Course.find(params[:id])
        if @course.update_attributes(entry_params)
            redirect_to courses_path
        else
            render 'edit'
        end
    end

    private
    def entry_params
        params.require(:course).permit(:user_ids =>[])
    end
end
